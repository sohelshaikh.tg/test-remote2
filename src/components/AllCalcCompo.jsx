import React from "react";
import { ButtonCom, Label, FormInputs } from "./common";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";

function AllButtonJumbled() {
  const [calc, setCalc] = useState("");
  const [result, setResult] = useState("");

  const ops = ["/", "*", "+", "-", ".", "*", "%", "+/-"];

  const updataCalc = (value) => {
    if (
      (ops.includes(value) && calc === "") ||
      (ops.includes(value) && ops.includes(calc.slice(-1)))
    ) {
      return;
    }
    setCalc(calc + value);
    if (!ops.includes(value)) {
      setResult(eval(calc + value).toString());
    }
  };

  const calculate = (value) => {
    if (!ops.includes(value) && calc === "") {
      return;
    }
    setCalc(eval(calc).toString());
  };

  const speacialops = (value) => {
    if (!ops.includes(value)) {
      setCalc("-" + calc);
    }
  };
  return (
    <>
      <div className="col-4 input-div">
        <Label
          heading
          content={result || "0"}
          placeholder="Add Some Input"
          fontsize="14px"
          color="pink"
          textalign="right"
        />
        <FormInputs
          placeholder={calc || "0"}
          typeOfInput="text"
          borderbottom="0"
          margin="0"
          placeholdercolor="red"
          color="white"
          width="100%"
          textalign="right"
          fontsize="2rem"
          height="80px"
        />
      </div>
      <div className="col-4">
        <ButtonCom
          className="common-width"
          value="%"
          onClick={() => updataCalc("%")}
        />
        <ButtonCom className="common-width" value="+/-" onClick={speacialops} />
        <ButtonCom
          className="common-width"
          value="C"
          onClick={() => setCalc("") & setResult("")}
        />
        <ButtonCom
          className="common-width diff-color"
          value="/"
          onClick={() => updataCalc("/")}
        />
      </div>
      <div className="col-4">
        <ButtonCom
          className="common-width"
          value="9"
          onClick={() => updataCalc("9")}
        />
        <ButtonCom
          className="common-width"
          value="8"
          onClick={() => updataCalc("8")}
        />
        <ButtonCom
          className="common-width"
          value="7"
          onClick={() => updataCalc("7")}
        />
        <ButtonCom
          className="common-width diff-color"
          value="X"
          onClick={() => updataCalc("*")}
        />
      </div>
      <div className="col-4">
        <ButtonCom
          className="common-width"
          value="6"
          onClick={() => updataCalc("6")}
        />
        <ButtonCom
          className="common-width"
          value="5"
          onClick={() => updataCalc("5")}
        />
        <ButtonCom
          className="common-width"
          value="4"
          onClick={() => updataCalc("4")}
        />
        <ButtonCom
          className="common-width diff-color"
          value="-"
          onClick={() => updataCalc("-")}
        />
      </div>
      <div className="col-4">
        <ButtonCom
          className="common-width"
          value="3"
          onClick={() => updataCalc("3")}
        />
        <ButtonCom
          className="common-width"
          value="2"
          onClick={() => updataCalc("2")}
        />
        <ButtonCom
          className="common-width"
          value="1"
          onClick={() => updataCalc("1")}
        />
        <ButtonCom
          className="common-width diff-color"
          value="+"
          onClick={() => updataCalc("+")}
        />
      </div>
      <div className="col-4">
        <ButtonCom
          className="common-width"
          value="0"
          onClick={() => updataCalc("0")}
        />
        <ButtonCom
          className="common-width"
          value=","
          onClick={() => updataCalc(".")}
        />
        <ButtonCom
          className="Result-btton different-color"
          value="="
          onClick={calculate}
        />
      </div>
    </>
  );
}

export default AllButtonJumbled;
