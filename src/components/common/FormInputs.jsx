import React from "react";
import styled from "@emotion/styled";

const FormInputStyled = styled.input`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  font-size: ${(props) => props.fontsize || "1rem"};
  border: none;
  background: none;
  outline: none;
  margin-top: 0px;
  border-bottom: ${(props) => props.borderbottom || "1px solid #828282"};
  margin: ${(props) => props.margin || "50px 0 0;"}
  color: ${(props) => props.color};
  text-align: ${(props) => props.textalign || "left"};

  &::placeholder {
    font-weight: ${(props) => props.placeholderweight};
    font-size: ${(props) => props.placeholderfontsize};
    line-height: 19px;
    letter-spacing: 0.02em;
    color: ${(props) => props.placeholdercolor || "rgba(80, 80, 80, 0.4);"};
  }
`;

function FormInputs({
  className = "",
  typeOfInput = "text",
  placeholder = "Write something",
  height = "",
  width = "",
  placeholderweight = "",
  placeholderfontsize = "",
  borderbottom = "",
  margin = "",
  placeholdercolor = "",
  color = "",
  textalign = "",
  fontsize = "",
  onChange,
}) {
  return (
    <FormInputStyled
      type={typeOfInput}
      placeholder={placeholder}
      height={height}
      width={width}
      placeholderweight={placeholderweight}
      placeholderfontsize={placeholderfontsize}
      className={`input-field ${className ? className : "message-box"}`}
      borderbottom={borderbottom}
      onChange={onChange}
      margin={margin}
      placeholdercolor={placeholdercolor}
      color={color}
      textalign={textalign}
      fontsize={fontsize}
    />
  );
}

export default FormInputs;
