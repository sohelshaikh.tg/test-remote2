import React from "react";
import styled from "@emotion/styled";
import "bootstrap/dist/css/bootstrap.min.css";

const LabelHeading = styled.h1`
  font-weight: ${(props) => props.weight};
  font-size: ${(props) => props.fontsize};
  line-height: 0px;
  color: ${(props) => props.color || "black"};
  position: ${(props) => props.position};
  margin-top: 18px;
  margin-bottom: 22px;
  text-align: ${(props) => props.textalign || "left"};
`;

const LabelPara = styled.p`
  font-weight: ${(props) => props.weight};
  font-size: ${(props) => props.fontsize};
  line-height: 45px;
  letter-spacing: 0.02em;
  color: ${(props) => props.color || "black"};
`;

const Label = ({
  heading = false,
  content = "",
  contentHeading = false,
  color = "",
  fontsize = "",
  weight = "",
  position = "",
  textalign = "",
}) => {
  if (heading) {
    return (
      <div className="container">
        <LabelHeading
          color={color}
          fontsize={fontsize}
          weight={weight}
          position={position}
          textalign={textalign}
        >
          {content}
        </LabelHeading>
      </div>
    );
  }

  if (contentHeading)
    return (
      <LabelPara
        color={color}
        fontsize={fontsize}
        weight={weight}
        textalign={textalign}
      >
        {content}
      </LabelPara>
    );
};

export default Label;
