import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import styled from "@emotion/styled";

const ButtonStyled = styled.button`
  padding: ${(props) => props.padding || "30px 9px"};
  border: none;
  outline: none;
  background-color: ${(props) => props.backgroundcolor};
  font-size: ${(props) => props.fontsize || "30px"};

  &:active {
    box-shadow: inset 0px 0px 80px 0px rgba(0, 0, 0, 0.25);
  }
  &:hover {
    background-color: rgb(159, 233, 169);
  }
`;

function ButtonCom({
  className = "",
  value = "",
  backgroundcolor = "",
  padding = "",
  onClick = "",
  fontsize = "",
}) {
  return (
    <ButtonStyled
      className={className}
      backgroundcolor={backgroundcolor}
      padding={padding}
      onClick={onClick}
      fontsize={fontsize}
    >
      {value}
    </ButtonStyled>
  );
}

export default ButtonCom;
