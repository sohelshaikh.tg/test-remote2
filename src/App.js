import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import "./App.css";
import { ButtonCom, Label, FormInputs, AllCalcCompo } from "./components/index";

function App() {
  return (
    <div className="container">
      <div className="row d-flex justify-content-center flex-column align-items-center">
        <AllCalcCompo />
      </div>
    </div>
  );
}

export default App;
